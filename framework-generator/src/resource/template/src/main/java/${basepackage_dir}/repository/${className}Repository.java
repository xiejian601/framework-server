package ${basepackage}.repository;

<#assign className=table.className>
<#assign classNameLower=className?uncap_first>
<#include"/java_imports.include">
import org.springframework.stereotype.Repository;

@Repository
public class ${className}Repository extends BaseMyBatisRepository<${className}, ${table.idColumn.javaType}>{
	
	public Page<${className}> findByPageRequest(PageRequest pr) {
		return pageQuery("${className}.paging", pr);
	}
	
	<#list table.columns as column>
	<#if column.unique && !column.pk>
	public ${className} getBy${column.columnName}(${column.javaType} v) {
		return sqlSession.selectOne("${className}.getBy${column.columnName}",v);
	}	
	</#if>
	</#list>
}