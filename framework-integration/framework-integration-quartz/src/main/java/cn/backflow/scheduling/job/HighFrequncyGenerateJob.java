package cn.backflow.scheduling.job;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

/**
 * 高频彩 彩期生成任务
 *
 * @author backflow
 * @since 2018-06-17
 */
public class HighFrequncyGenerateJob extends QuartzJobBean {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    private static final LocalTime start = LocalTime.of(8, 25);


    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

    }


    public static void main(String[] args) {

        LocalDateTime datetime = LocalDateTime.of(2018, 6, 15, 21, 30);

        Map<String, LocalDateTime> generate = generate(datetime, 10, 4, 81);
        generate.forEach((k, v) -> {
            System.out.println(k + ": " + v.format(formatter));
        });
    }

    /**
     * 生成场次开始时间
     *
     * @param when      指定开始生成的时间
     * @param minutes   时间间隔(分钟)
     * @param amount    生成的场数
     * @param maxPerDay 每天最大场数限制
     * @return 生成的时间列表
     */
    private static Map<String, LocalDateTime> generate(LocalDateTime when, int minutes, int amount, int maxPerDay) {
        Map<String, LocalDateTime> map = new TreeMap<>();

        // 计算分钟差
        // 计算场次
        long until = start.until(when, ChronoUnit.MINUTES);
        long t = until / 10;

        LocalDateTime date = LocalDateTime.from(when);
        LocalTime time = start.plusMinutes(t * minutes);
        t++;
        while (map.size() < amount) {
            t++;
            time = time.plusMinutes(minutes);

            // 超过最大期号时，生成下一天的期号
            if (t > maxPerDay) {
                t = 1;
                date = date.plusDays(1).with(time);
                time = start.plusMinutes(0);
                String key = keyFromDate(date, t);
                ;
                map.put(key, date.minusDays(1));
                continue;
            }

            date = date.with(time);
            String key = keyFromDate(date, t);
            map.put(key, date);
        }
        return map;
    }

    private static String keyFromDate(LocalDateTime date, long no) {
        return date.format(DateTimeFormatter.BASIC_ISO_DATE) + padLeft(no + "", 2, '0');
    }

    // 按给定时间得到场次
    private static int noFromGivenTime(Date when) {
        LocalDateTime then = LocalDateTime.ofInstant(when.toInstant(), ZoneId.systemDefault());
        return noFromGivenTime(then);
    }

    private static int noFromGivenTime(LocalDateTime when) {
        return noFromGivenTime(when.toLocalTime());
    }

    private static int noFromGivenTime(LocalTime when) {
        // 计算分钟差
        long until = start.until(when, ChronoUnit.MINUTES);
        // 与10求余+1为当时的场次
        long no = until / 10 + 1;
        if (no > 81 || no < 1) {
            no = 1;
        }
        return (int) no;
    }

    public static String padRight(String s, int n, char car) {
        return String.format("%1$-" + n + "s", s).replace(" ", String.valueOf(car));
    }

    public static String padLeft(String s, int n, char car) {
        return String.format("%1$" + n + "s", s).replace(" ", String.valueOf(car));
    }

}
