package cn.backflow.scheduling.job;

import cn.backflow.scheduling.service.JobService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.time.LocalDateTime;

@SuppressWarnings("all")
public class PrintCurrentTimeJob extends QuartzJobBean {

    private static Logger log = LoggerFactory.getLogger(PrintCurrentTimeJob.class);

    private JobService jobService;
    private String name;

    // Inject "JobService" bean
    @Autowired
    public void setJobService(JobService jobService) {
        this.jobService = jobService;
    }

    // Inject the "name" job data property
    public void setName(String name) {
        this.name = name;
    }

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println(jobService);
        log.info("{} executeInternal at {}", name, LocalDateTime.now());
    }
}
