package cn.backflow.secure.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 在Controller的方法上使用此注解，表示该方法在调用前会对用户进行身份验证，验证失败会返回401错误
 * <p>
 * 也可以直接在Controller上使用，代表该Controller的所有方法均需要身份验证
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Authentication {
}
